import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import "./PokemonItem.css"

const PokemonItem = (props) => {

    const pokemon = props.pokemon

    function getId() {
        return pokemon.url.split('/')[6]
    }

    return (
        <div className="pokemon-item">
            <NavLink to={'/pokemon/'+ getId()}>
                <img className="pokemon-item__img" src={pokemon.img}/>
                <div className="pokemon-item__name" >{pokemon.name ? pokemon.name[0].toUpperCase() + pokemon.name.slice(1) : pokemon.name}</div>
            </NavLink>
        </div>
    );
};

export default PokemonItem;