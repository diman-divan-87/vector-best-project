import React, {useState} from 'react';
import {setLimit} from "../../redux/reducers";
import {useDispatch, useSelector} from "react-redux";
import "./inputLimit.css"

const InputLimit = () => {
    const dispatch = useDispatch()
    const limit = useSelector(state => state.pokemon.limit)
    const [limitValue, setLimitValue] = useState(limit)
    const [limitError, setLimitError] = useState("")

    const limitHandler = (e) => {
        if (e == '' || e.charAt(0) == '0') {
            setLimitError("invalid value")
        } else {
            setLimitError("")
        }
        setLimitValue(prev => /\d+/.test(Number(e)) ? e : prev)
    }

    function submitLimitItems() {
         dispatch(setLimit(limitValue))
    }

    return (
        <div className="input-limit">
            <div className="input-limit__label">Set Limit</div>
            <input
                className="input-limit__input"
                name='limit'
                type="text"
                value={limitValue}
                onChange={(e) => limitHandler(e.target.value)}/>
            <div className={limitError ? "input-limit__btn limit__btn--disabled" : "input-limit__btn"} onClick={() => submitLimitItems()}>Apply</div>
            {(limitError) && <div className="input-limit__error">{limitError}</div>}
        </div>
    );
};

export default InputLimit;