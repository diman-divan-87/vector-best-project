import React from 'react';
import {setCurrentPage} from "../../redux/reducers";
import {useDispatch, useSelector} from "react-redux";
import {createPages} from "../../utils/pagesCreator";
import "./pagination.css"

const Pagination = () => {
    const dispatch = useDispatch()
    const limit = useSelector(state => state.pokemon.limit)
    const totalCount = useSelector(state => state.pokemon.totalCount)
    const currentPage = useSelector(state => state.pokemon.currentPage)
    const countPages = Math.ceil(totalCount / limit)

    const pages = []

    createPages(pages, countPages, currentPage)

    return (
            <div className="pages">
                <div
                    className={currentPage <= 1 ? "page page--disabled" : "page"}
                    onClick={() => dispatch(setCurrentPage(1))}>&#171;</div>
                <div
                    className={currentPage <= 1 ? "page page--disabled" : "page"}
                    onClick={() => dispatch(setCurrentPage((currentPage - 1) <= 1 ? 1 : currentPage - 1))}>&#8249;</div>
                {pages.map((page, index) => <div
                    key={index}
                    className={currentPage == page ? "current-page" : "page"}
                    onClick={() => dispatch(setCurrentPage(page))}>{page}</div>)}
                <div
                    className={currentPage >= countPages ? "page page--disabled" : "page"}
                    onClick={() => dispatch(setCurrentPage((currentPage + 1) >= countPages ? countPages : currentPage + 1))}>&#8250;</div>
                <div
                    className={currentPage >= countPages ? "page page--disabled" : "page"}
                    onClick={() => dispatch(setCurrentPage(countPages))}>&#187;</div>
            </div>
    );
};

export default Pagination;