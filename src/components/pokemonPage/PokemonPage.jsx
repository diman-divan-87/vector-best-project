import React, {useEffect} from 'react';
import {NavLink, useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getPokemonInfo} from "../../redux/action";
import "./pokemonPage.css"
import {resetLoadPokemon, setLoadPokemon, setPokemonInfo} from "../../redux/reducers";

const PokemonPage = (props) => {
    const {navigation} = props;
    const location = useLocation()
    const dispatch = useDispatch()
    const pokemonInfo = useSelector(state => state.pokemon.pokemonInfo)
    const isLoadPokemon = useSelector(state => state.pokemon.isLoadPokemon)

    useEffect(() => {
        dispatch(getPokemonInfo(location.pathname.split('/')[2]))
    }, [])

    function resetPokemon() {
        dispatch(resetLoadPokemon())
        dispatch(setLoadPokemon(true))
    }
    return (
        <div>
            {isLoadPokemon && <div className="pokemon-page-loading"></div>}
            {!isLoadPokemon &&
                <div>
                    <div className="pokemon-page-header">
                        <NavLink to={'/'}>
                            <div
                                className="pokemon-page-header__back"
                                onClick={() => resetPokemon()}
                            >Back</div>
                        </NavLink>
                        <div
                            className="pokemon-page-header__name">{pokemonInfo.name ? pokemonInfo.name[0].toUpperCase() + pokemonInfo.name.slice(1) : pokemonInfo.name}</div>
                    </div>
                    <div className="pokemon-page-info">
                        <img className="pokemon-page-info__img" src={pokemonInfo.img}/>
                        <div className="pokemon-page-info__specification">
                            <div className="pokemon-page-info__param">Height: {pokemonInfo.height}</div>
                            <div className="pokemon-page-info__param">Weight: {pokemonInfo.weight}</div>
                            <div className="pokemon-page-info__param">Abilities:</div>
                            <div className="pokemon-page-info__subparam">
                                {pokemonInfo.abilities.map((a, i) =>
                                    <div className="pokemon-page-info__subparam-item" key={i}>{a.ability.name}</div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>}
        </div>
    );
};

export default PokemonPage;