import React from 'react';
import './header.css'

const Header = () => {
    return (
        <div className="header">
            <a target="_blank" href="https://pokeapi.co/"><img src={require('./logo.png')} height={40} /></a>
            <a target="_blank" href="https://pokeapi.co/"><div className="header__name">The RESTful Pokémon API</div></a>
        </div>
    );
};

export default Header;