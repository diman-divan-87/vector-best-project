import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getPokemons} from "../redux/action";
import PokemonItem from "./pokemonItem/PokemonItem";
import {createPages} from "../utils/pagesCreator";
import InputLimit from "./inputLimit/InputLimit";
import "./main.css"
import Pagination from "./pagination/Pagination";
import {setFetching} from "../redux/reducers";

const Main = () => {
    const dispatch = useDispatch()
    const pokemons = useSelector(state => state.pokemon.items)
    const limit = useSelector(state => state.pokemon.limit)
    const currentPage = useSelector(state => state.pokemon.currentPage)
    const isFetching = useSelector(state => state.pokemon.isFetching)

    useEffect(() => {
        dispatch(getPokemons(currentPage, limit))
    }, [currentPage, limit])

    return (
        <div>
            <div>
                <InputLimit/>
                <Pagination/>
                {isFetching &&
                    <div className="pokemon__loading">
                        <div>Loading...</div>
                        <img className="rot" src={require("./pokeball.png")}/>
                    </div>
                }
                {!isFetching && <div className="pokemon-list">
                    {pokemons.map((pokemon, i) => <PokemonItem pokemon={pokemon} key={i}/>)}
                </div>}
                <Pagination/>
            </div>

        </div>
    );
};

export default Main;