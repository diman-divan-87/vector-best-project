import React from 'react';
import './App.css';
import Main from "./components/Main";
import PokemonPage from "./components/pokemonPage/PokemonPage";
import {BrowserRouter, Route, Routes, Navigate} from "react-router-dom";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Routes>
                    <Route exact path="/" element={<Main/>}/>
                    <Route path="/pokemon/:id" element={<PokemonPage/>}/>
                    <Route path="*" element={<Navigate to="/"/>} />
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
