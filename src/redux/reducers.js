const SET_POKEMON = "SET_POKEMON"
const SET_POKEMON_INFO = "SET_POKEMON_INFO"
const SET_CURRENT_PAGE = "SET_CURRENT_PAGE"
const SET_CURRENT_URL_POKEMON = "SET_CURRENT_URL_POKEMON"
const SET_LIMIT = "SET_LIMIT"
const SET_LOAD_POKEMON = "SET_LOAD_POKEMON"
const SET_FETCHING = "SET_FETCHING"
const RESET_POKEMON_INFO = "RESET_POKEMON_INFO"

const defaultState = {
    items: [],
    currentPage: 1,
    currentIdPokemon: '',
    isLoadPokemon: false,
    pokemonInfo: {
        abilities: [],
        height: 0,
        weight: 0,
        name: '',
        img: ''
    },
    perPage: 10,
    isFetching: true,
    totalCount: 0,
    limit: 100,
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case SET_POKEMON:
            return {
                ...state,
                items: action.payload.results,
                totalCount: action.payload.count
            }
        case SET_POKEMON_INFO:
            return {
                ...state,
                pokemonInfo: {
                    abilities: action.payload.abilities,
                    height: action.payload.height,
                    weight: action.payload.weight,
                    name: action.payload.name,
                    img: action.payload.sprites.other.dream_world.front_default ? action.payload.sprites.other.dream_world.front_default : action.payload.sprites.front_default
                }
            }
        case RESET_POKEMON_INFO:
            return {
                ...state,
                pokemonInfo: {
                    abilities: [],
                    height: 0,
                    weight: 0,
                    name: "",
                    img: ""
                }
            }
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.payload,
            }
        case SET_CURRENT_URL_POKEMON:
            return {
                ...state,
                currentIdPokemon: action.payload,
            }
        case SET_FETCHING:
            return {
                ...state,
                isFetching: action.payload,
            }
        case SET_LOAD_POKEMON:
            return {
                ...state,
                isLoadPokemon: action.payload,
            }
        case SET_LIMIT:
            return {
                ...state,
                limit: action.payload,
            }
        default:
            return state
    }
}

export const setPokemons = (pokemon) => ({type: SET_POKEMON, payload: pokemon})
export const setPokemonInfo = (pokemon) => ({type: SET_POKEMON_INFO, payload: pokemon})
export const setCurrentPage = (page) => ({type: SET_CURRENT_PAGE, payload: page})
export const setFetching = (bool) => ({type: SET_FETCHING, payload: bool})
export const setLimit = (limit) => ({type: SET_LIMIT, payload: limit})
export const setLoadPokemon = (bool) => ({type: SET_LOAD_POKEMON, payload: bool})

export const resetLoadPokemon = () => ({type: RESET_POKEMON_INFO})