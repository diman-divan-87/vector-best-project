import axios from 'axios'
import {setPokemons, setPokemonInfo, setLoadPokemon, setFetching} from "./reducers";

export const getPokemons = (currentPage, limit) => {
    return async (dispatch) => {
        dispatch(setFetching(true))
        console.log("dis 1")
        const response = await axios.get(`https://pokeapi.co/api/v2/pokemon?limit=${(limit)}&offset=${((currentPage-1)*limit)}`)
        const res = {
            results: [],
            count: response.data.count,
        }

        for(const r of response.data.results) {
            const d = await axios.get(r.url)
            res.results.push({name: r.name, url: r.url, img: d.data.sprites.front_default})
        }
        dispatch(setPokemons(res))
        dispatch(setFetching(false))
    }
}

export const getPokemonInfo = (currentId) => {
    return async (dispatch) => {
        dispatch(setLoadPokemon(true))
        const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${currentId}`)
        dispatch(setPokemonInfo(response.data))
        dispatch(setLoadPokemon(false))
    }
}